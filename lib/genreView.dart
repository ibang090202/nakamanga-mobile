import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import './detailManga.dart';

class DetailGenre extends StatelessWidget {
  final String namaGenre;
  final int index;
  DetailGenre({this.index,this.namaGenre});

  Future<List> getData() async {
    final response =
        await http.get("http://nakamamg.000webhostapp.com/api/genre/"+namaGenre);
    return json.decode(response.body)["komik"];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo[600],
        title: Text(namaGenre),
      ),
      body: new FutureBuilder<List>(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? new MangaList(
                  list: snapshot.data,
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}

class MangaList extends StatelessWidget {
  final List list;
  MangaList({this.list});

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return new Container(
          padding: const EdgeInsets.all(10.0),
          child: new GestureDetector(
            onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => DetailKomik(
                      idKomik: list[i]['id'],
                      judulKomik: list[i]['judul_komik'],
                      sinopsis: list[i]['sinopsis'],
                      cover: list[i]['cover'],
                      author: list[i]['author'],
                      status: list[i]['status'],
                      tahun: list[i]['tahun'],
                      views: list[i]['views'],
                    ),
                  ),
                );
              },
            child: new Card(
              color: Colors.indigo[400],
              child: new ListTile(
                title: new Text(list[i]['judul_komik'],
                style: TextStyle(color: Colors.white)),
              ),
            ),
          ),
        );
      },
    );
  }
}