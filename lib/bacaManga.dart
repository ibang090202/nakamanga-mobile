import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BacaManga extends StatelessWidget {

  final int idCh;
  final int ch;
  final int idKomik;
  final String judulKomik;

  const BacaManga({Key key, this.idCh, this.ch, this.judulKomik, this.idKomik}) : super(key: key);

  Future<List> getGambar() async {
    final response = await http.get(
        "http://nakamamg.000webhostapp.com/api/gambar/" + idCh.toString());
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo[600],
        title: Text(judulKomik),
      ),
      body: new FutureBuilder<List>(
          future: getGambar(),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);
            return snapshot.hasData
                ? new GambarList(
                    list: snapshot.data,
                    ch: ch,
                    idKomik: idKomik,
                  )
                : new Center(
                    child: new CircularProgressIndicator(),
                  );
          },
        ),
    );
  }
}

class GambarList extends StatelessWidget {
  final List list;
  final int ch, idKomik;

  const GambarList({Key key, this.list, this.ch, this.idKomik}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return Container(
          child: Image.network(
            'http://nakamamg.000webhostapp.com/data_gambar/komik-'+idKomik.toString()+'/ch-'+ch.toString()+'/'+list[i]['nama_gambar'],
            fit: BoxFit.fill,
          ),
        );
      },
    );
  }
}