import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './detailManga.dart';

class Manga extends StatelessWidget {
  Future<List> getData() async {
    final response =
        await http.get("http://nakamamg.000webhostapp.com/api/komik");
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new FutureBuilder<List>(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? new ItemList(
                  list: snapshot.data,
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}

class ItemList extends StatelessWidget {
  final List list;
  ItemList({this.list});
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return new Container(
          margin: EdgeInsets.all(15.0),
          child: new Card(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8.0))),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => DetailKomik(
                      idKomik: list[i]['id'],
                      judulKomik: list[i]['judul_komik'],
                      sinopsis: list[i]['sinopsis'],
                      cover: list[i]['cover'],
                      author: list[i]['author'],
                      status: list[i]['status'],
                      tahun: list[i]['tahun'],
                      views: list[i]['views'],
                    ),
                  ),
                );
              },
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0),
                    ),
                    child: Image.network(
                      "http://nakamamg.000webhostapp.com/data_gambar/cover/"+list[i]['cover'],
                      height: 250.0,
                      fit: BoxFit.cover,
                      loadingBuilder: (BuildContext context, Widget child,
                          ImageChunkEvent loadingProgress) {
                        if (loadingProgress == null) return child;
                        return Container(
                          height: 200.0,
                          child: Center(
                              child: CircularProgressIndicator(
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                    loadingProgress.expectedTotalBytes
                                : null,
                          )),
                        );
                      },
                    ),
                  ),
                  ListTile(
                  title: Text(
                    list[i]['judul_komik'],
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                 )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}