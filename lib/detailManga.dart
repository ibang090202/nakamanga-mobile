import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import './bacaManga.dart';

class DetailKomik extends StatelessWidget {
  final int idKomik;
  final String judulKomik;
  final String sinopsis;
  final String cover;
  final String author;
  final String status;
  final int tahun;
  final int views;

  const DetailKomik(
      {Key key,
      this.idKomik,
      this.judulKomik,
      this.sinopsis,
      this.cover,
      this.author,
      this.status,
      this.tahun,
      this.views})
      : super(key: key);

  Future<List> getChapter() async {
    final response = await http.get(
        "http://nakamamg.000webhostapp.com/api/chapter/" + idKomik.toString());
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo[600],
        title: Text(judulKomik),
      ),
      drawer: Drawer(
        child: new FutureBuilder<List>(
          future: getChapter(),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);
            return snapshot.hasData
                ? new ChapterList(
                    list: snapshot.data,
                    idKomik: idKomik,
                    judulKomik: judulKomik,
                  )
                : new Center(
                    child: new CircularProgressIndicator(),
                  );
          },
        ),
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
              child: new Hero(
            tag: judulKomik,
            child: new Material(
              child: new InkWell(
                child: new Image.network(
                  "http://nakamamg.000webhostapp.com/data_gambar/cover/" +
                      cover,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          )),
          new BagNama(
            judulKomik: judulKomik,
          ),
          new Auth(
            author: author,
          ),
          new Stats(
            status: status,
          ),
          new Year(
            tahun: tahun.toString(),
          ),
          new View(
            views: views.toString(),
          ),
          new Sinopsis(),
          new Desc(
            sinopsis: sinopsis,
          ),
        ],
      ),
    );
    //
  }
}

class BagNama extends StatelessWidget {
  BagNama({this.judulKomik});
  final String judulKomik;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(judulKomik,
              style: new TextStyle(fontSize: 20.0, color: Colors.blue)),
        ],
      ),
    );
  }
}

class Auth extends StatelessWidget {
  Auth({this.author});
  final String author;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(
            "Author :    ",
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
          new Text(author,
              style: TextStyle(color: Colors.grey[600], fontSize: 16.0)),
        ],
      ),
    );
  }
}

class Stats extends StatelessWidget {
  Stats({this.status});
  final String status;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(
            "Status :    ",
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
          new Text(status,
              style: TextStyle(color: Colors.grey[600], fontSize: 16.0)),
        ],
      ),
    );
  }
}

class Year extends StatelessWidget {
  Year({this.tahun});
  final String tahun;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(
            "Tahun Rilis :    ",
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
          new Text(tahun,
              style: TextStyle(color: Colors.grey[600], fontSize: 16.0)),
        ],
      ),
    );
  }
}

class View extends StatelessWidget {
  View({this.views});
  final String views;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(
            "Views :    ",
            style: TextStyle(color: Colors.black, fontSize: 16.0),
          ),
          new Text(views,
              style: TextStyle(color: Colors.grey[600], fontSize: 16.0)),
        ],
      ),
    );
  }
}

class Sinopsis extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text("Sinopsis :    ",
              style: new TextStyle(fontSize: 16.0, color: Colors.black)),
        ],
      ),
    );
  }
}

class Desc extends StatelessWidget {
  Desc({this.sinopsis});
  final String sinopsis;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.all(10.0),
      child: new Card(
        child: new Padding(
          padding: const EdgeInsets.all(10.0),
          child: new Text(
            sinopsis,
            style: new TextStyle(fontSize: 16.0, color: Colors.grey[600]),
            textAlign: TextAlign.justify,
          ),
        ),
      ),
    );
  }
}

class ChapterList extends StatelessWidget {
  final List list;
  final int idKomik;
  final String judulKomik;

  ChapterList({this.list, this.judulKomik, this.idKomik});

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: list == null ? 0 : list.length,
      itemBuilder: (context, i) {
        return Container(
          child: new Column(
            children: <Widget>[
              // padding: const EdgeInsets.all(10.0),
              Padding(
                padding: const EdgeInsets.all(10.0),
              ),
              new GestureDetector(
                onTap: () => Navigator.of(context).push(
                  new MaterialPageRoute(
                      builder: (BuildContext context) => new BacaManga(
                            idCh: list[i]['id'],
                            ch: list[i]['ch'],
                            idKomik: idKomik,
                            judulKomik: judulKomik,
                          )),
                ),
                child: new Card(
                  color: Colors.indigo[400],
                  child: new ListTile(
                    title: new Text(list[i]['ch'].toString(),
                        style: TextStyle(color: Colors.white)),
                    leading: new Text("Chapter",
                        style: TextStyle(color: Colors.white, fontSize: 16.0)),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
