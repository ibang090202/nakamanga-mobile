import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import './detailManga.dart';

class Home extends StatelessWidget {
  Future<List> getData() async {
    final response =
        await http.get("http://nakamamg.000webhostapp.com/api/kUpdate");
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new FutureBuilder<List>(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? new MangaList(
                  list: snapshot.data,
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}

class MangaList extends StatelessWidget {
  final List list;
  MangaList({this.list});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.indigo[900],
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: GridView.builder(
          itemCount: list == null ? 0 : list.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 5.5,
            crossAxisSpacing: 4.5,
            childAspectRatio: 0.71
          ),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => DetailKomik(
                      idKomik: list[index]['id'],
                      judulKomik: list[index]['judul_komik'],
                      sinopsis: list[index]['sinopsis'],
                      cover: list[index]['cover'],
                      author: list[index]['author'],
                      status: list[index]['status'],
                      tahun: list[index]['tahun'],
                      views: list[index]['views'],
                    ),
                  ),
                );
              },
              child: Hero(
                tag: list[index]['judul_komik'],
                
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    'http://nakamamg.000webhostapp.com/data_gambar/cover/'+list[index]['cover'],
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

