import 'package:flutter/material.dart';
import 'package:nakamangaa/searchView.dart';

import './genre.dart' as genre;
import './home.dart' as home;
import './manga.dart' as manga;
import './searchManga.dart';

int _selectedIndex = 1;
void main() {
  runApp(new MaterialApp(
    home: new Index(),
  ));
}

class Index extends StatefulWidget {
  @override
  _IndexState createState() => _IndexState();
}

class _IndexState extends State<Index> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    controller = new TabController(vsync: this, length: 3);
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.indigo[600],
        title: new Text("Nakamanga"),
        leading: new IconButton(
          icon: new Icon(Icons.search, color: Colors.white),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new Search()));
          },
        ),
      ),
      body: new IndexedStack(
        index: _selectedIndex,
        children: <Widget>[
          new genre.Genre(),
          new home.Home(),
          new manga.Manga()
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
        backgroundColor: Colors.indigo[600],
        unselectedItemColor: Colors.white30,
        showSelectedLabels: true,
        showUnselectedLabels: false,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text('Genre'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text('Manga'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}
